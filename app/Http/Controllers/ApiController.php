<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use GuzzleHttp\Client;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Exception\ParseException;
use Carbon\Carbon;
use Cache;

class ApiController extends BaseController
{

    public function __construct(Client $client)
    {
      //setup some global variables to use later on
      $this->client = $client;
      $this->e2_settings = config('app.e2');
    }

    public function index()
    {
      //setup the current date and 3 days from now
      $date_from = date("Y-m-d")."T00:00:00Z";
      $date_to   = date('Y-m-d', strtotime('+3 days', strtotime($date_from)))."T23:59:59Z";
      $page      = 1;

      $this->matches = [];
      $this->match_dates = [];
      //get all the matches as a bug array
      //structure is date -> importance
      $this->getMatches($page, $date_from, $date_to);

      //looping through matches array
      foreach ($this->match_dates as $key => $data_array) {
        foreach ($data_array as $matches) {
          // sorting matches within the same importance group by time
          usort($matches, function($a, $b) {
            return $a['time'] <=> $b['time'];
          });
        }

        // sorting the matches by importance
        krsort($this->match_dates[$key]);
      }

      return view('list', ['matches' => $this->match_dates]);
    }

    /**
    * Get details about competition
    *
    * @return void
    */
    protected function getMatches($page, $date_from = "", $date_to = "")
    {
      if ($page == 1) {
        //initial call to get the first page of the matches
        $response = $this->sendRequest($this->e2_settings['api_url'] . '/v3/de_DE/42/matches?apikey=' . $this->e2_settings['api_key'] . '&attach=matches.competition&matchdate_from=' . $date_from . '&matchdate_to=' . $date_to . '&states=PRE');
      } else {
        //rest of the call is based on the link found in the call
        $response = $this->sendRequest($this->e2_settings['api_url'] . $page . '&apikey=' . $this->e2_settings['api_key']);
      }

      //if we have response data we have to process it
      if (!empty($response) && is_array($response)) {
          foreach ($response['data'] as $data) {
            //make sure we have attachments data se we will have all the details we need
            if (isset($response['attachments'][$data])) {
              $explode = explode("/", $data);
              $match_id = $explode[count($explode)-1];
              $this->matches[$match_id] = $response['attachments'][$data];
              //get info for competition
              $this->getCompetition($response, $data, $match_id);

              //get info for teams
              $this->getTeams($response, $data, $match_id);

              $match_date = date('Y-m-d', strtotime($this->matches[$match_id]['matchdate']));
              //create array based on date and importance
              if (isset($this->matches[$match_id]['competition']['globalImportance'])) {
                  $this->match_dates[$match_date][$this->matches[$match_id]['competition']['globalImportance']][] = [
                      'time'        => date('H:i', strtotime($this->matches[$match_id]['matchdate'])),
                      'country'     => $this->matches[$match_id]['competition']['country'],
                      'competition' => $this->matches[$match_id]['competition']['name'],
                      'teams'       => $this->matches[$match_id]['teams']
                  ];
              }
            }
          }
      }

      //if we have next page, we have to call the same again
      if (isset($response['pagination']['next'])) {
        $this->getMatches($response['pagination']['next'], "", "");
      }
    }

    /**
    * Get details about competition
    *
    * @return void
    */
    protected function getCompetition($response, $data, $match_id)
    {
      if (isset($response['attachments'][$data]['competition'])) {
        $competition_id = $response['attachments'][$data]['competition'];
        if (isset($response['attachments'][$competition_id])) {
          $this->matches[$match_id]['competition'] = $response['attachments'][$competition_id];
        }

        if (isset($this->matches[$match_id]['competition']['country'])) {
          $country_response = $this->sendRequest($this->e2_settings['api_url'] . $this->matches[$match_id]['competition']['country'] . '?apikey=' . $this->e2_settings['api_key']);
          if (isset($country_response['data']['name'])) {
            $this->matches[$match_id]['competition']['country'] = $country_response['data']['name'];
          }
        }
      }
    }

    /**
    * Get details about competition
    *
    * @return void
    */
    protected function getTeams($response, $data, $match_id)
    {
      if (isset($response['attachments'][$data]['teams']['home'])) {
        $home_team_id = $response['attachments'][$data]['teams']['home'];
        if (isset($response['attachments'][$home_team_id])) {
          $this->matches[$match_id]['teams']['home'] = $response['attachments'][$home_team_id];
        }
      }

      if (isset($response['attachments'][$data]['teams']['away'])) {
        $away_team_id = $response['attachments'][$data]['teams']['away'];
        if (isset($response['attachments'][$away_team_id])) {
          $this->matches[$match_id]['teams']['away'] = $response['attachments'][$away_team_id];
        }
      }
    }

    /**
    * Send the API request
    *
    * @return bool|\SimpleJSONElement
    */
    protected function sendRequest($api_url, $headers = null, $auth = null, $body = null, $post = false)
    {
        $cacheKey = str_slug($api_url, '_');

        // attemp to get response from Cache
        $response_array = Cache::get($cacheKey);
        if (empty($response_array)) {
          if (empty($headers)) {
              $headers = array(
                  'Accept' => 'application/json',
              );
          }

          $options = [
              'headers' => $headers,
          ];

          // if (!empty($auth)) {
          //     $options['auth'] = $auth;
          // }

          try {
              if ($post && $post === "POST") {
                // POST requests
              } elseif ($post && $post === "PUT") {
                // PATCH requests
              } elseif ($post && $post === "DELETE") {
                // DELETE requests
              } else {
                // GET requests
                  $response = $this->client->get($api_url, $options);
              }

              $response_array = json_decode($response->getBody()->getContents(), true);

              //save response to cache - only save real response, not failure
              $expiresAt = Carbon::now()->addMinutes(60);
              Cache::put($cacheKey, $response_array, $expiresAt);

              return $response_array;
          } catch (\GuzzleHttp\Exception\RequestException $e) {
              $response = $e->getResponse();
              $error    = $e->getMessage().PHP_EOL.$e->getRequest()->getMethod();

              Log::error('RequestException: '.$error);
              Log::error('RequestException Response: '.print_r($error, true));
              return false;
          } catch (\GuzzleHttp\Exception\ClientException $e) {
              $response = $e->getResponse();
              $error    = 'ClientException: '.$e->getMessage();

              Log::error('ClientException: '.$error);
              return false;
          } catch (\Exception $e) {
              $response = $e->getResponse();
              $error    = $e->getMessage().PHP_EOL.$e->getRequest()->getMethod();

              Log::error('Exception: '.$error);
              return false;
          }//end try
        } else {
          return $response_array;
        }

        return false;
    }//end sendRequest()
}
